/**
 *
 *@AUTHOR       :   PRATIK VARSHNEY
 *@DATE         :   16-09-2014
 *@FACULTY      :   11-PEB-002
 *@ENROLLMENT   :   GE0004
 *@ROLLNUMBER   :   1
 *@TOPIC        :   Image Compression Using RLE and Fixed length encoding
 *
 */

#include<cstdio>
#include<iostream>
#include<vector>
#include<algorithm>
#include<fstream>
#include<cmath>

using namespace std;

struct image_data
{
    int width;
    int height;
    int depth;
    int size;
    char type[3];
    unsigned char *pixel;
};

struct node
{
    bool val;
    int freq;
};

struct freq
{
    int val;
    int freq;
};

int load_image(struct image_data *input_image, const char *path);
int rle(struct image_data *input_image, const char *path);

ofstream debug_file("debug.txt");

bool compare_fn(struct freq i, struct freq j) { return ((i.freq)<(j.freq)); }

int main(int argc, char *argv[])
{
    int i;
    string in_file, out_file;
    char waitkey;

    struct image_data input_image;

    if(argc==1)
    {
        cout << "Enter input PGM filename : ";
        cin >> in_file;

        cout << "\nEnter output filename : ";
        cin >> out_file;
    }
    else if(argc>2)
    {
        in_file = string(argv[1]);
        out_file = string(argv[2]);
    }
    if(argc==2)
    {
        printf("\n!! Invalid Argument !!\n\n");
        printf("Command Example:\n%s <input.pgm> <output.bin>\n\nAUTHOR  : \tPRATIK VARSHNEY\nFACULTY : \t11-PEB-002\n\n", argv[0]);
        return -1;
    }

    load_image(&input_image, in_file.c_str());

    printf("TYPE\t: %s\n", input_image.type);
    printf("WIDTH\t: %d\n", input_image.width);
    printf("HEIGHT\t: %d\n", input_image.height);
    printf("DEPTH\t: %d\n", input_image.depth);
    printf("BYTES\t: %d\n", input_image.size);

    cout << "\nGenerating DEBUG info...\n";

    debug_file << "DEBUG FILE\n\nAUTHOR : PRATIK VARSHNEY\n\n";

    debug_file << "TYPE\t: " << input_image.type << endl;
    debug_file << "WIDTH\t: " << input_image.width<< endl;
    debug_file << "HEIGHT\t: " << input_image.height<< endl;
    debug_file << "DEPTH\t: " << input_image.depth<< endl;
    debug_file << "BYTES\t: " << input_image.size<< endl;

    rle(&input_image, out_file.c_str());

    cout << "\nOUTPUT FILE : " << out_file << " saved";

    cout << "\n\n--------------------------------------------------------------------------------\n";
    cout <<     "                             Output Format Info                                 \n";
    cout <<     "--------------------------------------------------------------------------------\n";
    cout << "PRATIK" << endl;
    cout << "<width> <height>" << endl;
    cout << "<depth>" << endl;
    cout << "<first bit of raw input>" << endl;
    cout << "<dictionary size>" << endl;
    cout << "<dictionary index entries : equivalent of 0, 1, 2, etc. in encoded data>" << endl;
    cout << "<encoded data>" << endl;
    cout <<   "\n--------------------------------------------------------------------------------\n";

    fflush(stdin);
    cout << "\nPress Enter to continue...\n";
    scanf("%c", &waitkey);


    return 0;
}

int load_image(struct image_data *input_image, const char *path)
{
    FILE *fp;
    char temp;
    int i;
    unsigned int val;

    fp=fopen(path, "rb");
    fgets(input_image->type, 3, fp);

    while(1)
    {
        temp=fgetc(fp);
        /* skip until new line | ASCII=10 */
        if(temp==10) break;
        if(temp==' ') break;
    }
    while(1)
    {
        temp=fgetc(fp);
        if(temp=='#')
        {
            do
            {
                temp=fgetc(fp);
                /* skip until new line | ASCII=10 */
            }while(temp!=10);
        }
        else
        {
            fseek(fp, -1, 1);
            break;
        }
    }

    fscanf(fp, "%d%d%d", &(input_image->width), &(input_image->height), &(input_image->depth));

    do
    {
        temp=fgetc(fp);
        /* skip until new line | ASCII=10 */
    }while(temp!=10);

    input_image->size = (input_image->width) * (input_image->height);

    input_image->pixel = new unsigned char[input_image->size];

    switch(input_image->type[1])
    {
    case '2':
        for(i=0; i<(input_image->size); i++)
        {
            fscanf(fp, "%u", &val);
            input_image->pixel[i] = (unsigned char)val;
        }
        break;
    case '5':
        fread(input_image->pixel, sizeof(unsigned char), input_image->size, fp);
        break;
    }

    fclose(fp);

    return 0;
}

int rle(struct image_data *input_image, const char *path)
{
    int i, j;
    unsigned char mask;
    bool curr;
    vector<struct node> a;
    struct node *temp;
    temp = new struct node;
    temp->freq=0;
    temp->val=((input_image->pixel[0]) & 0b10000000)!=0;
    for(i=0; i<(input_image->size); i++)
    {
        mask = 0b10000000;
        while(mask>0)
        {
            curr = ((input_image->pixel[i]) & mask)!=0;
            if(curr!=(temp->val))
            {
                a.push_back(*temp);
                temp = new struct node;
                temp->freq=1;
                temp->val=curr;
            }
            else
            {
                temp->freq = temp->freq + 1;
            }
            mask = mask >> 1;
        }
    }
    a.push_back(*temp);

    debug_file << endl;
    debug_file << "Bit Frequencies\n(Bit, Frequency)\n";
    for(vector<struct node>::iterator it=a.begin(); it!=a.end(); it++)
    {
        debug_file << it->val << ", " << it->freq << endl;
    }

    vector<struct freq> b;
    struct freq *t;
    t = new struct freq;
    t->freq = 0;
    t->val = (a[0]).freq;

    b.push_back(*t);

    for(vector<struct node>::iterator it=a.begin(); it!=a.end(); it++)
    {
        bool found=0;
        for(vector<struct freq>::iterator it2=b.begin(); it2!=b.end(); it2++)
        {
            if(it->freq == it2->val)
            {
                found=1;
                it2->freq = it2->freq + 1;
                break;
            }
        }
        if(!found)
        {
            t = new struct freq;
            t->freq = 1;
            t->val = it->freq;
            b.push_back(*t);
        }
    }

    /*cout << "\n\n-----\n\n";
    for(vector<struct freq>::iterator it2=b.begin(); it2!=b.end(); it2++)
    {
        cout << it2->val << ", " << it2->freq << endl;
    }*/

    sort(b.begin(), b.end(), compare_fn);
    reverse(b.begin(), b.end());

    debug_file << "\n\n-----\n\nFrequencies of Frequencies\n(Value, Frequency)\n\n";
    for(vector<struct freq>::iterator it2=b.begin(); it2!=b.end(); it2++)
    {
        debug_file << it2->val << ", " << it2->freq << endl;
    }

    ofstream outp(path);
    outp << "PRATIK\n" << input_image->width << " " << input_image->height << "\n" << input_image->depth << "\n";
    unsigned char s = (unsigned char)((a[0]).val) + '0';
    outp << s << "\n";
    outp << b.size() << "\n";
    vector<struct freq>::iterator it2=b.begin();
    outp << it2->val;
    it2++;
    for(; it2!=b.end(); it2++)
    {
        outp << " " << it2->val;
    }
    outp << "\n";

    int num = ceil(log(b.size())/log(2));
    int max_mask = 1, temp_mask;
    max_mask = max_mask << (num-1);

    vector<bool> bstr(num*a.size());
    vector<bool>::iterator itb=bstr.begin();
    for(vector<struct node>::iterator it=a.begin(); it!=a.end(); it++)
    {
        for(i=0; i<b.size(); i++)
        {
            if(it->freq == (b[i]).val)
            {
                j=max_mask;
                while(j>0)
                {
                    *itb=((i & j)!=0);
                    itb++;
                    j = j >> 1;
                }
            }
        }
    }
    cout << "\nNEW\t: " << ceil(bstr.size()/8) << endl;
    for(i=0; i<bstr.size(); i=i+8)
    {
        unsigned char fstr=0;
        unsigned char fmsk=0b10000000;
        for(j=0; j<8; j++)
        {
            if(i+j < bstr.size())
            {
                fstr += (fmsk * bstr[i+j]);
                fmsk = fmsk >> 1;
                //cout << bstr[i+j];
            }
            else break;
        }
        outp << fstr;
    }

    return 0;
}
